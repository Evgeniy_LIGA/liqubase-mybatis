1. Заходим в Postgre.
 
        sudo  -u postgres psql
  
2. И создаем БД test_database.

        CREATE DATABASE test_database;

3. Подключаемся к БД test_database в Intellij. Тестируем соединение с БД.


4. Правим все properties в папке src/main/resources/db/...

       username=postgres
       password=12345
       driver=org.postgresql.Driver
       url=jdbc:postgresql://localhost:5432/test_database

0. Правим src/main/resources/application.yml

       url: jdbc:postgresql://localhost:5432/test_database
       password: 12345
       username: postgres

0. В src/main/resources/db/changelog/1.0.1/sql/01__create_account_table.sql копируем:
   
        create sequence if not exists person_seq start 1;
        create sequence if not exists employment_seq start 1;
        create table person
        (
        person_id   bigint default nextval('person_seq'::regclass) not null
        constraint person_pk
        primary key,
        first_name  varchar(256)                                   not null,
        last_name   varchar(256)                                   not null,
        middle_name varchar(256)                                   not null,
        birth_date  date,
        gender      varchar(100)                                   not null
        );
        comment on table person is 'Инфорамция о человеке';
        comment on column person.person_id is 'идентификатор.';
        comment on column person.first_name is 'Имя';
        comment on column person.last_name is 'Фамилия';
        comment on column person.middle_name is 'Фамилия';
        comment on column person.birth_date is 'Дата рождения';
        comment on column person.gender is 'Пол';
        create table employment
        (
        employment_id        bigint default nextval('employment_seq'::regclass) not null
        constraint employment_pk
        primary key,
        version              integer                                            not null,
        start_dt             date,
        end_dt               date,
        work_type_id         bigint,
        organization_name    varchar(256),
        organization_address text,
        position_name        varchar(256),
        person_id            bigint
        );    
        comment on table employment is 'запись о трудовой деятельности.';
        comment on column employment.employment_id is 'идентификатор.';
        comment on column employment.version is 'реализация оптимистической блокировки';
        comment on column employment.start_dt is 'дата начала трудовой деятельности';
        comment on column employment.end_dt is 'дата окончания трудовой деятельности';
        comment on column employment.work_type_id is 'тип деятельности';
        comment on column employment.organization_name is 'наименование организации.';
        comment on column employment.organization_address is 'адрес организации.';
        comment on column employment.position_name is 'должность.';
        alter table employment
        add constraint person_id_fk foreign key (person_id) references person (person_id);

0. Проверка статуса миграции. В терминал вводим:
   
        mvn -Pdev liquibase:status -Dliquibase.verbose
   
   Результат: BUILD SUCCESS


8. Применение миграции. В терминал вводим:
        
        mvn -Pdev liquibase:update  

   Результат: BUILD SUCCESS


9. Посмотреть SQL запросы, которые выполнятся при миграции
    
        mvn -Pdev liquibase:updateSQL

   Результат: BUILD SUCCESS


10. В IntelliJ нажимаем "обновить базы данных". И проверяем наличие
   новых таблиц в БД test_database.

# Ну а дальше само задание

1. Создаем DTO employment и Mapper в DAO.
   
   
2. Обращаем внимание, на то, что название полей в запросах отличается от 
   названий полей в таблице. Следовательно, в dto с помощью аннотаций 
   прописываем эти различия иначе mapper не найдет эти поля и будет игнорировать их.
   
   
3. Если нужно снести последовательность в SQL используем:

         -- снести последовательность и все данные что с ней связанны
         DROP SEQUENCE employment_seq CASCADE;
         -- создать последовательность
         create sequence if not exists employment_seq start 1;
         -- удалить все.
         DELETE FROM employment WHERE employment_id notnull ;

4. Не забываем указать путь к запросу:

         @PostMapping("/api/employment/records/update")

5. Запрос отправляем через rest.http. Запрос:

         POST http://localhost:8080/api/employment/records/update
         Content-Type: application/json

         [
            {
               "startDt": "2020-10-04",
               "endDt": "2020-12-04",
               "organizationName": "ООО ЭйТи Консальтинг",
               "positionName": "Разработчик",
               "personId": "1"
            },{
               "employmentId": "1",
               "startDt": "2020-08-04",
               "endDt": "2020-10-03",
               "organizationName": "ООО ЭйТи Консальтинг",
               "positionName": "Тестировщик",
               "personId": "1"
            },{
               "employmentId": "2",
               "startDt": "2020-08-04",
               "endDt": "2020-10-03",
               "organizationName": "ООО ЭйТи Консальтинг",
               "positionName": "Аналитик",
               "personId": "1"
            }
         ]
