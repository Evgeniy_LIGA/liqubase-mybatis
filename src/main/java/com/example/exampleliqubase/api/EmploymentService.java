package com.example.exampleliqubase.api;

import java.util.List;

import com.example.exampleliqubase.dto.EmploymentDTO;

// Это интерфейсы для воздействия из вне.

public interface EmploymentService {

    EmploymentDTO update(EmploymentDTO accountDTO);

    void deletewithversionlqzero();

    void invertversion();

    List<EmploymentDTO> getListAccounts();

}
