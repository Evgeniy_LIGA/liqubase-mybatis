package com.example.exampleliqubase.controller;

import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dto.EmploymentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping()
public class EmploymentController {

    @Autowired
    private EmploymentService service;

    @PostMapping("/api/employment/records/update")
    public List<EmploymentDTO> update(@RequestBody @Validated List<EmploymentDTO> dto) {
        // для отладки
        //dto.forEach(t -> {System.out.println(t);});
        // Инвертируем версию
        service.invertversion();
        // обрабатываем запрос
        dto.forEach(t -> {service.update(t);});
        // удаляем все записи у которых version < 0 т.е. они не ново созданные и не обновленные
        service.deletewithversionlqzero();
        return null;
    }

}
