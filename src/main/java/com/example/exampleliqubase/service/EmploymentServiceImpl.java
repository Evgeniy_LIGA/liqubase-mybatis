package com.example.exampleliqubase.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dao.EmploymentMapper;
import com.example.exampleliqubase.dto.EmploymentDTO;

import com.example.exampleliqubase.model.EmploymentEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EmploymentServiceImpl implements EmploymentService {

    @Autowired
    private EmploymentMapper employmentMapper;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void init() {
        System.out.println();
    }

    @Override
    @Transactional
    public EmploymentDTO update(EmploymentDTO emploDTO) {

        // Переводим DTO в entity c помощью mapper
        EmploymentEntity emploEntity = modelMapper.map(emploDTO, EmploymentEntity.class);

        // Если Version null делаем её -1. из-за инверсии.
        if(emploEntity.getVersion()==null)emploEntity.setVersion(1);

        if(emploEntity.getEmployment_id()==null) {
            // если нет id создаем новую сущность
            employmentMapper.save(emploEntity);
        } else {
            // если id есть обновляем запись.
            employmentMapper.update(emploEntity);
        }

        //тут еще какая-то логика
        return modelMapper.map(emploEntity, EmploymentDTO.class);
    }

    @Override
    public void deletewithversionlqzero() {
        employmentMapper.deletewithversionzero();
    }

    @Override
    public void invertversion() {
        employmentMapper.invertversion();
    }

    @Override
    public List<EmploymentDTO> getListAccounts() {
       return employmentMapper.getListEmployments().stream()
                .map(accountEntity -> modelMapper.map(accountEntity, EmploymentDTO.class))
                .collect(Collectors.toList());
    }
}
