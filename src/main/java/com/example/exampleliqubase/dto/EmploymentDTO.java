package com.example.exampleliqubase.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Date;

// DTO — это объект, который определяет, как данные будут передаваться по сети.
// А entity это скорее всего то что будет записываться в БД.

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentDTO {
    @JsonProperty("employmentId")
    private Long employment_id;
    //private Integer version;
    @JsonProperty("startDt")
    private Date start_dt;
    @JsonProperty("endDt")
    private Date end_dt;
    //private Long work_type_id;
    @JsonProperty("organizationName")
    private String organization_name;
    //private String organization_address;
    @JsonProperty("positionName")
    private String position_name;
    @JsonProperty("personId")
    private Long person_id;
}
