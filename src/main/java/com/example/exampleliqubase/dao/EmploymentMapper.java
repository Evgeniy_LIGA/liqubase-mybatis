package com.example.exampleliqubase.dao;

import java.util.List;

import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.model.EmploymentEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface EmploymentMapper {

    @SelectKey(resultType = Long.class, keyProperty = "employment_id", before = true,
            statement = "select nextval('employment_seq')")
    @Insert("insert into employment "+
                    "( employment_id,    version,    start_dt,    end_dt,    organization_name,    position_name) " +
            "values (#{employment_id}, #{version}, #{start_dt}, #{end_dt}, #{organization_name} ,#{position_name} )")
    void save(EmploymentEntity accountEntity);

    // version = version-1 потому что версия инвертированная
    @Update("UPDATE employment SET "+
            "version = abs(version)+1, start_dt = #{start_dt}, end_dt = #{end_dt}, organization_name = #{organization_name}, "+
            "position_name = #{position_name} "+
            "WHERE employment_id = #{employment_id}")
    void update(EmploymentEntity accountEntity);

    @Delete("DELETE FROM employment WHERE version < 0")
    void deletewithversionzero();

    @Update("UPDATE employment SET version = -version")
    void invertversion();

    @Select("select * from account_entity_liquibase")
    List<EmploymentEntity>getListEmployments();
}
